import re
import copy
import logging

import eventlet
import socketio
import requests


WEB_SERVER_URL = 'https://digital-vanguard.herokuapp.com'


sio = socketio.Server()
clients = dict()
searching = {
    'Математика': list(),
    'Биология': list(),
    'Русский язык': list(),
    'Информатика': list(),
    'Химия': list(),
}


@sio.event
def connect(sid, environ):
    print(f'Client with {sid} connected.')
    clients[sid] = {
        'auth': False,
    }


@sio.event
def authenticate(sid, data):
    print(f'Authentication trial from {sid}')
    try:
        access_token = data['access_token']
    except KeyError:
        sio.emit('auth_result', data={'result': 'failure'}, room=sid)
        return

    headers = {
        'Authorization': f'Bearer {access_token}'
    }
    response = requests.get(WEB_SERVER_URL + '/account', headers=headers)
    print(f'Request to {WEB_SERVER_URL}/account.'
          f' Response: {response.status_code}')

    if response.status_code == 200:
        sio.emit('auth_result', data={'result': 'success'}, room=sid)
        json_data = response.json()
        clients[sid]['auth'] = True
        clients[sid]['token'] = access_token
        clients[sid]['username'] = json_data['username']
        clients[sid]['fighter_level'] = json_data['fighter_level']
        clients[sid]['state'] = 'idle'
    else:
        sio.emit('auth_result', data={'result': 'failure'}, room=sid)


@sio.event
def start_battle(sid, data):
    print(f'Starting battle from {sid}')
    try:
        battle_type = data['battle_type']
        subject = data['subject']
    except KeyError:
        print('Could not find type of subject')
        return

    print('Battle type: ' + battle_type)

    if battle_type == 'training':
        username = clients[sid]['username']
        response = requests.get(
            WEB_SERVER_URL + '/training',
            params={'username': username, 'subject': subject})

        json_data = response.json()
        clients[sid]['tasks'] = json_data['tasks']
        clients[sid]['flags'] = [False] * len(json_data['tasks'])
        clients[sid]['state'] = 'training'
        clients[sid]['subject'] = subject
        sio.emit(
            'fight', {
                'tasks': json_data['tasks'],
                'self': {
                    'username': username,
                    'fighter_level': clients[sid]['fighter_level'],
                },
                'opponent': {
                    'username': 'Scarecrow',
                    'fighter_level': 1,
                },
            }, room=sid)
    elif battle_type == 'battle':
        srch = searching[subject]
        if sid not in srch:
            srch.append(sid)
        print(srch)
        if len(srch) > 1:
            sid_1 = srch.pop(0)
            sid_2 = srch.pop(0)
            response = requests.get(
                WEB_SERVER_URL + '/battle',
                params={
                    'username_1': clients[sid_1]['username'],
                    'username_2': clients[sid_2]['username'],
                    'subject': subject,
                })
            print(f'Response to /battle. Status code: {response.status_code}')
            tasks = response.json()['tasks']
            sids = [sid_1, sid_2]
            print(sids)
            for sid in sids:
                clients[sid]['tasks'] = copy.deepcopy(tasks)
                clients[sid]['flags'] = [False] * len(tasks)
                clients[sid]['state'] = 'fighting'
                clients[sid]['subject'] = subject
            clients[sid_1]['opponent'] = sid_2
            clients[sid_2]['opponent'] = sid_1
            for sid in sids:
                opponent = clients[sid]['opponent']
                print(f'Emit fight for {sid}')
                sio.emit(
                    'fight', {
                        'tasks': tasks,
                        'self': {
                            'username': clients[sid]['username'],
                            'fighter_level': clients[sid]['fighter_level'],
                        },
                        'opponent': {
                            'username': clients[opponent]['username'],
                            'fighter_level': clients[opponent]['fighter_level'],
                        },
                    }, room=sid)


@sio.event
def submit_task(sid, data):
    if clients[sid]['state'] == 'idle':
        print('Cannot accept task from idle player')
        return

    print(f'Received task from {sid}')
    try:
        task_index = data['task_index']
        answer = data['answer']
    except KeyError:
        print(f'Received task caused error')
        return

    if not clients[sid]['flags'][task_index]:
        clients[sid]['flags'][task_index] = True
        task = clients[sid]['tasks'][task_index]
        task['correctness'] = task['answer'] == answer
        print('Task answered '
            + 'correctly' if task['correctness'] else 'incorrectly')
        sio.emit(
            'submitted', {
                'index': task_index,
                'correct': task['correctness'],
                'source': 'you',
            }, room=sid)

        if clients[sid]['state'] == 'fighting':
            print('Submit for opponent also')
            opponent = clients[sid]['opponent']
            print(f'SID: {sid}, opponent: {opponent}')
            sio.emit(
                'submitted', {
                    'index': task_index,
                    'correct': task['correctness'],
                    'source': 'opponent',
                }, room=opponent)

        if sum(clients[sid]['flags']) == len(clients[sid]['tasks']):
            print('End of game condition')
            points = 0
            for task in clients[sid]['tasks']:
                points += int(task['correctness'])
            print(f'{sid} has {points} correct answers')

            if clients[sid]['state'] == 'training':
                sio.emit(
                    'stop_battle', {
                        'winner': 'you'
                    }, room=sid)
            else:
                opp_points = 0
                for task in clients[opponent]['tasks']:
                    opp_points += int(task['correctness'])
                print(f'{opponent} has {opp_points} correct answers')

                sio.emit(
                    'stop_battle', {
                        'winner': 'you' if points >= opp_points else 'opponent'
                    }, room=sid)
                sio.emit(
                    'stop_battle', {
                        'winner': 'you' if opp_points > points else 'opponent'
                    }, room=opponent)

            sids = [sid]
            if clients[sid]['state'] == 'fighting':
                sids.append(opponent)

            for sid in sids:
                data = {
                    'subject': dict()
                }
                try:
                    subject = data['subject']
                    subject['name'] = clients[sid]['subject']
                    subject['tasks'] = list()
                    for task in clients[sid]['tasks']:
                        subject['tasks'].append({
                            'category': task['category'],
                            'correctness': int(task['correctness'])})
                except Exception:
                    print('Some error occured')
                    return

                headers = {
                    'Authorization': f'Bearer {clients[sid]["token"]}'
                }
                response = requests.post(WEB_SERVER_URL + '/time_series',
                                            json=data, headers=headers)
                print(f'Made response to {WEB_SERVER_URL}/time_series.'
                        f' Response code: {response.status_code}')

                exp_gained = 0
                for task in clients[sid]['tasks']:
                    exp_gained += int(task['correctness'])
                response = requests.post(
                    WEB_SERVER_URL + f'/experience/{exp_gained}',
                    headers=headers)
                print(f'Made response to {WEB_SERVER_URL}/experience.'
                        f' Response code: {response.status_code}')

                clients[sid]['state'] = 'idle'
                del clients[sid]['subject']
                del clients[sid]['tasks']
                del clients[sid]['flags']
                if 'opponent' in clients[sid]:
                    del clients[sid]['opponent']


@sio.event
def disconnect(sid):
    print(f'Client with {sid} just disconnected.')
    if sid in clients:
        del clients[sid]


app = socketio.WSGIApp(sio)


if __name__ == '__main__':
    eventlet.wsgi.server(eventlet.listen(('', 3467)), app)
